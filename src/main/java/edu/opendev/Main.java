package edu.opendev;


import edu.opendev.guess.GameCore;
import edu.opendev.guess.GameWrapper;
import edu.opendev.guess.player.Player;

/**
 * типы данных
 * арифметические операции
 * операции отношения
 * битовые операции
 * классы Math и StrictMath
 * приведение типов
 */

public class Main {

    final static double pi = 3.14;//3.14D, 3.14F

    /**
     * Точка входа в приложение
     *
     * @param args параметры запуска
     */
    public static void main(String[] args) {

        int max = 100;
        /*GameCore gg = new GameCore(max, new StupidRndBot(max));
        gg.start();
        System.out.println("******************");

        gg.setRespondent(new DiBot(max));
        gg.start();
        System.out.println("******************");*/

        GameCore gg = new GameCore(max, new Player("Игрок-человек"), 5);
        gg.start();
        /*GameWrapper gw = new GameWrapper(new GameCore(max, new Player("Tester"), 5));
        gw.start();*/
    }



}

