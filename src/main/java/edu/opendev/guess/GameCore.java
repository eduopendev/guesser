package edu.opendev.guess;

import edu.opendev.guess.player.Bot;
import edu.opendev.guess.player.DiBot;
import edu.opendev.guess.player.Player;
import edu.opendev.guess.player.Respondent;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by ralex on 18.08.16.
 */
public class GameCore {

    /**
     * Угадывающий (умеет давать предположительный ответ)
     */
    private Respondent respondent;

    /**
     * Состояние игры
     */
    private GameState state;

    private CommandHandler commandHandler;

    /**
     * Коллекция типа Map, для сопоставления результатов проверки и их текстового описания
     */
    private static final Map<ResultCheck, String> textMap = new HashMap<ResultCheck, String>() {
        {
            put(ResultCheck.LEFT, "мало");
            put(ResultCheck.RIGHT, "много");
            put(ResultCheck.MATCH, "Победа!");
            put(ResultCheck.GAMEOVER, "Вы проиграли!");
        }
    };

    /**
     * Тип перечисление для результатов проверки
     */
    public enum ResultCheck {
        LEFT, MATCH, RIGHT, GAMEOVER;

    }

    /**
     * Класс для описания результатов игры
     */
    public class GameResult {
        private final String playerName;
        private final GameState state;

        /**
         * Приватный конструкор, наружу результаты могут попасть только от обрамляющего класса
         *
         * @param playerName
         * @param state
         */
        private GameResult(String playerName, GameState state) {
            this.playerName = playerName;
            //здесь простое присваивание this.state = state не подходит, т.к. оно просто присвоит ссылку
            // на тот же объект, нужно сделать новый объект-копию переданного, и присвоить уже его
            this.state = new GameState(state);
        }

        public int getValue() {
            return state.value;
        }

        public int getCount() {
            return state.count;
        }

        public String getPlayerName() {
            return playerName;
        }
    }

    /**
     * Встроенный класс, описывает состояние игры
     */
    private class GameState {
        /**
         * верхнаяя граница (включительно) для загаданного числа
         */
        private int max;

        /**
         * загаданное число от 1 до {@link GameState#max}
         */
        private int value;

        /**
         * чисто совершенных попыток отгадывания
         */
        private int count = 0;

        /**
         * ограничение на число попыток
         */
        private int limit = 0;

        /**
         * Результаты проверки предыдущего ответа
         */
        private ResultCheck prevResultCheck = null;

        /**
         * Обязательный конструктор
         *
         * @param max
         * @param value
         */
        public GameState(int max, int value, int limit) {
            this.max = max;
            this.value = value;
            this.limit = limit;
        }

        public GameState(int max, int value) {
            this(max, value, 0);
        }

        /**
         * конструктор копирования
         *
         * @param state
         */
        public GameState(GameState state) {
            this.max = state.max;
            this.value = state.value;
            this.count = state.count;
            this.limit = state.limit;
        }

        public void reset() {
            this.count = 0;
            this.prevResultCheck = null;
        }
    }

    private class CommandHandler {

        private Map<String, Command> commands = new HashMap<>();

        private class Command {
            private final String description;
            private final Runnable runnable;

            private Command(String description, Runnable runnable) {
                this.description = description;
                this.runnable = runnable;
            }

            public String getDescription() {
                return description;
            }

            public void run() {
                runnable.run();
            }
        }


        private CommandHandler() {

            Command cmdExit = new Command("Выход", () -> System.exit(0));
            commands.put("exit", cmdExit);
            //commands.put("quit", cmdExit);

            Command cmdHelp = new Command("Список команд", () -> {
                for (Map.Entry<String, Command> entry : commands.entrySet()) {
                    System.out.println(new StringBuilder(entry.getKey())
                            .append(": ")
                            .append(entry.getValue().getDescription()));
                }
            });
            commands.put("help", cmdHelp);
            //commands.put("list", cmdHelp);

            Command cmdRemain = new Command("Остаток попыток", () -> {
                StringBuilder sb = new StringBuilder();
                if (state.limit == 0) {
                    sb.append("число попыток не ограничено");
                } else {
                    sb.append("Вы сделали попыток: ").append(state.count).append(", осталось еще ")
                            .append(state.limit - state.count);
                }
                System.out.println(sb);
            });
            commands.put("remain", cmdRemain);

            Command cmdRunDiBot = new Command("Запустить бота", new Runnable() {
                @Override
                public void run() {
                    Bot bot = new DiBot(state.max);
                    setRespondent(bot);
                }
            });
            commands.put("bot", cmdRunDiBot);

        }

        public boolean handle(String input) {
            String key = input.toLowerCase();
            if (commands.containsKey(key)) {
                commands.get(key).run();
            } else {
                System.out.println("ошибка ввода, укажите либо целое число, либо команду, например help");
            }
            return true;
        }
    }

    public static GameCore getInstance() {
        return new GameCore();
    }

    /**
     * Конструктор с полной инициализацией
     *
     * @param max
     * @param respondent
     */
    public GameCore(int max, Respondent respondent, int limit) {
        Random rnd = new Random();
        int value = rnd.nextInt(max) + 1;

        this.state = new GameState(max, value, limit);
        this.commandHandler = new CommandHandler();
        this.respondent = respondent;
    }

    public GameCore(int max, Respondent respondent) {
        this(max, respondent, 0);
    }

    /**
     * Конструктор по-умолчанию
     */
    public GameCore() {
        this(100, new Player("Аноним"), 0);
    }

    /**
     * Конструктор с частичной инициализацией
     *
     * @param max
     */
    public GameCore(int max) {
        this(max, new Player("Аноним"), 0);
    }


    public void setRespondent(Respondent respondent) {
        this.respondent = respondent;
    }

    /**
     * Проверяет число, если оно меньше, больше или равно загаданному, вернет соответственно ResultCheck.LEFT,
     * ResultCheck.RIGHT или ResultCheck.MATCH
     * Обновляет поле {@link GameState#prevResultCheck }
     *
     * @param value
     * @return
     */
    public ResultCheck checkValue(int value) {
        ResultCheck r = ResultCheck.MATCH;
        if (value < state.value) {
            r = ResultCheck.LEFT;
        } else if (value > state.value) {
            r = ResultCheck.RIGHT;
        }
        state.count++;
        if (state.count == state.limit && r != ResultCheck.MATCH) {
            r = ResultCheck.GAMEOVER;
        }
        state.prevResultCheck = r;
        return r;
    }

    /**
     * перед началом игры
     */
    private void before() {
        System.out.println(new StringBuilder("Приветствую, ").append(respondent.getName()).append("!").toString());
        StringBuilder sb = new StringBuilder("Я загадал число от 1 до ").append(state.max).append(", отгадайте его");
        if (state.limit > 0) {
            sb.append(" (число попыток ").append(state.limit).append(")");
        }
        System.out.println(sb.toString());
        state.reset();
        respondent.init();
    }

    /**
     * Начать играть
     */
    public GameResult start() {
        before();
        String answer;
        ResultCheck resultCheck;
        do {
            answer = respondent.nextAnswer(state.prevResultCheck);
            resultCheck = handleAnswer(answer);
        } while (!(resultCheck == ResultCheck.MATCH || resultCheck == ResultCheck.GAMEOVER));
        GameResult result = new GameResult(respondent.getName(), state);
        after(result);
        return result;
    }

    private ResultCheck handleAnswer(String answer) {
        ResultCheck resultCheck = null;
        if (isDigits(answer)) {
            resultCheck = checkValue(Integer.parseInt(answer));
            printResultCheck(resultCheck, answer);
        } else {
            commandHandler.handle(answer);
        }
        return resultCheck;
    }

    private static boolean isDigits(String str) {
        boolean ret = true;
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                ret = false;
                break;
            }
        }
        return ret;

    }

    /**
     * после завершения игры
     *
     * @param result
     */
    private void after(GameResult result) {
        System.out.println("Игра завершена, " + result.getPlayerName());
        System.out.printf("Вы совершили %d попыток, загададное число: %d%n", result.getCount(), result.getValue());
    }

    private void printResultCheck(ResultCheck resultCheck, String answer) {
        //if (this.respondent instanceof Player) {
            System.out.println(answer + " " + textMap.get(resultCheck));
        //}
    }

}
