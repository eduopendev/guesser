package edu.opendev.guess.player;

import edu.opendev.guess.GameCore;

/**
 * Created by ralex on 31.08.16.
 */
public class DiBot extends Bot {

    private int prevAnswer;
    private int left;
    private int right;

    public DiBot(int max) {
        super(max);
        init();
    }

    @Override
    public String getName() {
        return "Бот-дихотомист";
    }

    @Override
    public String nextAnswer(GameCore.ResultCheck prevResultCheck) {
        int answer;

        if (prevResultCheck == GameCore.ResultCheck.LEFT) {
            left = prevAnswer;
        } else if (prevResultCheck == GameCore.ResultCheck.RIGHT) {
            right = prevAnswer;
        }

        answer = left + (right - left + 1) / 2;

        prevAnswer = answer;
        return String.valueOf(answer);
    }

    @Override
    public void init() {
        left = 0;
        right = max;
        prevAnswer = 0;
    }
}
