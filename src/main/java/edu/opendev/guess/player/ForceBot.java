package edu.opendev.guess.player;

import edu.opendev.guess.GameCore;

/**
 * Created by ralex on 05.09.16.
 */
public class ForceBot extends Bot {

    private int answer;

    public ForceBot(int max) {
        super(max);
        init();
    }

    @Override
    public String getName() {
        return "Бот-брутфорс";
    }

    @Override
    public String nextAnswer(GameCore.ResultCheck prevResultCheck) {
        return String.valueOf(++answer);
    }

    @Override
    public void init() {
        answer = 0;
    }
}
