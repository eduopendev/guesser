package edu.opendev.guess.player;

import edu.opendev.guess.GameWrapper;
import edu.opendev.guess.GameCore;

import java.util.Scanner;
import java.util.function.Supplier;

/**
 * Created by ralex on 23.08.16.
 */
public class Player implements Respondent {

    private String name;

    public Player(String name) {
        this.name = name;
    }

    public Player() {
        this.name = "Аноним";
    }

    @Override
    public String getName() {
        return name;
    }

    public String nextAnswer(GameCore.ResultCheck prevResultCheck) {
        System.out.println("ваш ответ:");
        Scanner in = new Scanner(System.in);
        String answer = in.nextLine();
        return answer;
    }

    @Override
    public void init() {

    }

}
