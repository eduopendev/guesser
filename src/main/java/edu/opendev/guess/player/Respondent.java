package edu.opendev.guess.player;

import edu.opendev.guess.GameCore;

/**
 * Created by ralex on 23.08.16.
 */
public interface Respondent {
    String getName();
    String nextAnswer(GameCore.ResultCheck prevResultCheck);
    void init();
}
