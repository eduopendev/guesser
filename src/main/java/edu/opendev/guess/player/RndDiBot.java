package edu.opendev.guess.player;

import edu.opendev.guess.GameCore;

/**
 * Created by ralex on 05.09.16.
 */
public class RndDiBot extends RndBot {

    private int prevAnswer;
    private int left;
    private int right;

    public RndDiBot(int max) {
        super(max);
        init();
    }

    @Override
    public String getName() {
        return "Бот-гибрид";
    }

    @Override
    public String nextAnswer(GameCore.ResultCheck prevResultCheck) {
        int answer;

        if (prevResultCheck == GameCore.ResultCheck.LEFT) {
            left = prevAnswer;
        } else if (prevResultCheck == GameCore.ResultCheck.RIGHT) {
            right = prevAnswer;
        }

        answer = left + rnd.nextInt(right - left) + 1;

        prevAnswer = answer;
        return String.valueOf(answer);
    }

    @Override
    public void init() {
        left = 0;
        right = max;
        prevAnswer = 0;
    }
}
