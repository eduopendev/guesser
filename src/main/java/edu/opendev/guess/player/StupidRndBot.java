package edu.opendev.guess.player;

import edu.opendev.guess.GameCore;

/**
 * Created by ralex on 31.08.16.
 */
public class StupidRndBot extends RndBot {

    public StupidRndBot(int max) {
        super(max);
    }

    @Override
    public String getName() {
        return "Бот-тупой рандом";
    }

    @Override
    public String nextAnswer(GameCore.ResultCheck prevResultCheck) {
        int answer = rnd.nextInt(max+1);
        return String.valueOf(answer);
    }

    @Override
    public void init() {

    }


}
